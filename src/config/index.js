import dotenv from 'dotenv';

dotenv.config();

const SERVER = {
  PROTOCOL: process.env.PROTOCOL || 'http',
  PORT: process.env.PORT || 3000,
};

const SPOTIFY = {
  CALLBACK_URL: process.env.SPOTIFY_CALLBACK_URL,
  CLIENT_ID: process.env.SPOTIFY_CLIENT_ID,
  CLIENT_SECRET: process.env.SPOTIFY_CLIENT_SECRET,
};

const config = {
  SERVER,
  SPOTIFY,
};

export default config;
