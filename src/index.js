import express from 'express';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import querystring from 'querystring';
import axios from 'axios';

import config from './config';
// import spotify from './spotify';

// Load config settings
const SERVER_PORT = config.SERVER.PORT;
const { CALLBACK_URL, CLIENT_ID, CLIENT_SECRET } = config.SPOTIFY;
let ACCESS_TOKEN = null;
// let REFRESH_TOKEN = null;

const app = express();

// Apply Middleware
app
  .use(cors())
  .use(cookieParser())
  .use(express.json());

app.listen(SERVER_PORT, () => {
  console.log(`Listening on port ${SERVER_PORT}`);
});

// Specify Spotify scope details
app.get('/login', (req, res) => {
  const scope = [
    'user-read-private',
    'user-read-email',
    'user-read-playback-state',
    'user-read-currently-playing',
    'user-read-recently-played',
    'user-library-read',
  ].join(' ');

  const queryString = querystring.stringify({
    response_type: 'code',
    client_id: CLIENT_ID,
    scope,
    redirect_uri: CALLBACK_URL,
  });

  res.json({
    url: `https://accounts.spotify.com/authorize?${queryString}`,
  });
});

function getAuthHeader() {
  return `Basic ${Buffer.from(`${CLIENT_ID}:${CLIENT_SECRET}`)
    .toString('base64')}`;
}

app.post('/callback', async (req, res) => {
  // Extract Authorization Code from the request
  const { authCode } = req.body;

  // Request an Access Token using the Authorization Code
  try {
    const response = await axios.post('https://accounts.spotify.com/api/token',
      querystring.stringify({
        code: authCode,
        redirect_uri: 'http://local.silenz.io:8080/callback',
        grant_type: 'authorization_code',
      }), {
        headers: {
          Authorization: getAuthHeader(),
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      });

    ACCESS_TOKEN = response.data.access_token;
    // REFRESH_TOKEN = response.data.refresh_token;

    // use the access token to access the Spotify Web API
    await axios.get('https://api.spotify.com/v1/me', {
      headers: {
        Authorization: `Bearer ${ACCESS_TOKEN}`,
      },
    });

    res
      .status(200)
      .json({
        message: 'Authenticated successfully',
      });
  } catch (err) {
    res
      .status(400)
      .json({
        error: err,
      });
  }
});

// TODO: add support for token refresh
app.get('/refresh', () => {

});
